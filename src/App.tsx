import { createBrowserRouter, RouterProvider, Outlet } from 'react-router-dom';
import { Flex } from '@chakra-ui/react';

import { Navbar } from '@components/navbar';

import { Home } from '@pages/home';
import { Projects } from '@pages/projects';

const Layout = () => (
  <Flex direction="column" flex="1" bg="bg.surface">
    <Navbar />
    <Outlet />
  </Flex>
);

const router = createBrowserRouter([
  {
    path: '/',
    element: <Layout />,
    children: [
      {
        path: '/',
        element: <Home />,
      },
      {
        path: '/projects',
        element: <Projects />,
      },
    ],
  },
]);

const App = () => <RouterProvider router={router} />;

export default App;
