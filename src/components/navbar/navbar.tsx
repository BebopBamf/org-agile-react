import {
  Avatar,
  Box,
  Button,
  ButtonGroup,
  Container,
  HStack,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputLeftElement,
  useDisclosure,
  Popover,
  PopoverTrigger,
  PopoverContent,
  Stack,
  useColorMode,
  useColorModeValue,
} from '@chakra-ui/react';
import { Link as RRLink } from 'react-router-dom';
import { FiBell, FiSearch, FiSun, FiMoon } from 'react-icons/fi';

import { PopoverIcon } from './popover-icon';
import { Logo } from './logo';
import { MobileDrawer } from './mobile-drawer';

export const ProjectsPopover = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <Popover
      isOpen={isOpen}
      onOpen={onOpen}
      onClose={onClose}
      trigger="hover"
      openDelay={0}
    >
      <PopoverTrigger>
        <Button rightIcon={<PopoverIcon isOpen={isOpen} />}>Projects</Button>
      </PopoverTrigger>
      <PopoverContent p="2" maxW="fit-content">
        <Stack spacing="0" alignItems="stretch">
          <Button
            as={RRLink}
            to="/projects"
            variant="tertiary"
            justifyContent="start"
          >
            View Projects
          </Button>
          <Button
            as={RRLink}
            to="/projects"
            variant="tertiary"
            justifyContent="start"
          >
            Create Project
          </Button>
        </Stack>
      </PopoverContent>
    </Popover>
  );
};

export const Navbar = () => {
  const { toggleColorMode } = useColorMode();
  const ColorIcon = useColorModeValue(<FiMoon />, <FiSun />);

  return (
    <Box
      borderBottomWidth="1px"
      bg="bg.accent.default"
      position="relative"
      zIndex="tooltip"
      as="nav"
      role="navigation"
    >
      <Container py="4">
        <HStack justify="space-between" spacing="8">
          <HStack spacing="10">
            <HStack spacing="3">
              <MobileDrawer />
              <Logo />
            </HStack>
            <ButtonGroup
              size="lg"
              variant="text.accent"
              colorScheme="gray"
              spacing="8"
              display={{ base: 'none', lg: 'flex' }}
            >
              <Button as={RRLink} to="/">
                Home
              </Button>
              <ProjectsPopover />
            </ButtonGroup>
          </HStack>
          <HStack spacing={{ base: '2', md: '4' }}>
            <InputGroup
              maxW="2xs"
              display={{ base: 'none', md: 'inline-flex' }}
            >
              <InputLeftElement>
                <Icon as={FiSearch} color="fg.accent.muted" fontSize="lg" />
              </InputLeftElement>
              <Input placeholder="Search" variant="filled.accent" />
            </InputGroup>

            <ButtonGroup variant="tertiary.accent" spacing="1">
              <IconButton
                icon={<FiSearch />}
                aria-label="Search"
                display={{ base: 'flex', lg: 'none' }}
                isRound
              />
              <IconButton
                icon={ColorIcon}
                onClick={toggleColorMode}
                aria-label="Toggle Color Mode"
                isRound
              />
              <IconButton
                icon={<FiBell />}
                aria-label="Show notification"
                isRound
              />
            </ButtonGroup>
            <Avatar boxSize="10" src="https://i.pravatar.cc/300" />
          </HStack>
        </HStack>
      </Container>
    </Box>
  );
};
