import { createSlice } from '@reduxjs/toolkit';

import { RootState } from '../store';

export type Project = {
  id: string;
  name: string;
  description: string;
  createdAt: EpochTimeStamp;
};

export type ProjectState = {
  total: number;
  items: Project[];
};

// Dummy Projects - will delete
const initialState: ProjectState = {
  total: 10,
  items: [
    {
      id: 'cced3c05-210a-47f8-a495-0355d9f4c4cf',
      name: 'Project 1',
      description: 'This is the first project',
      createdAt: 1620000000,
    },
    {
      id: '290a7c08-ac24-4084-aa81-560417336674',
      name: 'Project 2',
      description: 'This is the second project',
      createdAt: 1620000001,
    },
    {
      id: 'fd5e11cf-3e9a-4784-9c31-70f2a83004ff',
      name: 'Project 3',
      description: 'This is the third project',
      createdAt: 1620000002,
    },
    {
      id: '7824fda0-6530-4e08-8b95-3e5b0c1d3b13',
      name: 'Project 4',
      description: 'This is the fourth project',
      createdAt: 1620000003,
    },
    {
      id: 'b2c89a4b-1873-46db-8c1d-3c9a0e080987',
      name: 'Project 5',
      description: 'This is the fifth project',
      createdAt: 1620000004,
    },
    {
      id: '1734f44e-6a3d-4f38-90c2-8d62d0f19b7f',
      name: 'Project 6',
      description: 'This is the sixth project',
      createdAt: 1620000005,
    },
    {
      id: '59dcb13d-b6a0-4304-bf8d-c84e6762829c',
      name: 'Project 7',
      description: 'This is the seventh project',
      createdAt: 1620000006,
    },
    {
      id: '8a82f4c7-51ff-42e3-8cfd-8e2a34490d10',
      name: 'Project 8',
      description: 'This is the eighth project',
      createdAt: 1620000007,
    },
    {
      id: '1e4ab6e5-868f-4c94-b9e3-6cbce965d06f',
      name: 'Project 9',
      description: 'This is the ninth project',
      createdAt: 1620000008,
    },
    {
      id: '9abce87e-bb31-43b2-ae08-64e28b1c7bf0',
      name: 'Project 10',
      description: 'This is the tenth project',
      createdAt: 1620000009,
    },
  ],
};

export const projectsSlice = createSlice({
  name: 'projects',
  initialState,
  reducers: {
    addProject: (state, action) => {
      state.items.push(action.payload);
      state.total++;
    },
  },
});

export const { addProject } = projectsSlice.actions;

export const selectAllProjects = (state: RootState) => state.projects.items;

export default projectsSlice.reducer;
