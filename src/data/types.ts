export type Project = {
  id: string;
  name: string;
  description: string;
  createdAt: EpochTimeStamp;
};

export type PaginatedData<T> = {
  data: T[];
  count: number;
  page: number;
  perPage: number;
};
