import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { Project, PaginatedData } from './types';

export const apiSlice = createApi({
  reducerPath: 'api',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:8080/api/v1' }),
  tagTypes: ['Projects'],
  endpoints: (builder) => ({
    getAllProjects: builder.query<PaginatedData<Project>, number>({
      query: (page) => `/projects?limit=5&offset=${5 * (page - 1)}`,
      providesTags: (result, _, _) =>
        result
          ? [
              ...result.data.map(({ id }) => ({ type: 'Projects' as const, id })),
              { type: 'Projects', id: 'PARTIAL-LIST' },
            ]
          : [{ type: 'Projects', id: 'PARTIAL-LIST' }],
    }),
    getProject: builder.query<Project, number>({
      query: (id) => `/projects/${id}`,
    }),
    invalidatesTags: (_, _, id) => [
      { type: 'Projects', id },
      { type: 'Projects', id: 'PARTIAL-LIST' },
    ],
  }),
});

export const { useGetAllProjectsQuery, useGetProjectQuery } = apiSlice;
