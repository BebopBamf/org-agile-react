import { useState } from 'react';
import {
  Box,
  Button,
  Container,
  Stack,
  Heading,
  Text,
  useBreakpointValue,
  InputGroup,
  ButtonGroup,
  HStack,
  Icon,
  Input,
  InputLeftElement,
  Checkbox,
  IconButton,
  Table,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
  Skeleton,
} from '@chakra-ui/react';
import { FiEdit2, FiSearch, FiTrash2 } from 'react-icons/fi';
import { map } from 'lodash/fp';
//import { IoArrowDown } from 'react-icons/io4';

import type { Project } from '../data/projects-slice';
import { useGetAllProjectsQuery } from '../data/api-slice';

const ProjectTableRow = ({ id, name, description }: Project) => (
  <Tr key={id}>
    <Td>
      <HStack spacing="3">
        <Checkbox />
        <Text color="fg.muted">{name}</Text>
      </HStack>
    </Td>

    <Td>
      <Text color="fg.muted">{description}</Text>
    </Td>

    <Td>
      <HStack spacing="1">
        <IconButton icon={<FiTrash2 />} variant="tertiary" aria-label="Delete member" />
        <IconButton icon={<FiEdit2 />} variant="tertiary" aria-label="Edit member" />
      </HStack>
    </Td>
  </Tr>
);

const ProjectTableRowLoading = (id: number) => (
  <Tr key={id}>
    <Td>
      <HStack spacing="3">
        <Checkbox />
        <Skeleton height="12px" w="50%" />
      </HStack>
    </Td>
    <Td>
      <Skeleton height="12px" w="100%" />
    </Td>
    <Td>
      <HStack spacing="1">
        <IconButton icon={<FiTrash2 />} variant="tertiary" aria-label="Delete member" />
        <IconButton icon={<FiEdit2 />} variant="tertiary" aria-label="Edit member" />
      </HStack>
    </Td>
  </Tr>
);

type ProjectsTableProps = {
  page: number;
};

const ProjectsTable = ({ page = 1 }: ProjectsTableProps) => {
  const { data: projects, isLoading, isError, error } = useGetAllProjectsQuery(page);

  if (isLoading) {
    <Tbody>
      {Array.from({ length: 5 }, (_, idx) => {
        return ProjectTableRowLoading(idx);
      })}
    </Tbody>;
  }

  if (isError) {
    console.error(error);
    return <Box>{error.toString()}</Box>;
  }

  return <Tbody>{map(ProjectTableRow)(projects?.data || [])}</Tbody>;
};

const ProjectsTableContainer = () => {
  const isMobile = useBreakpointValue({ base: true, md: false });

  const [page, setPage] = useState(1);
  const { data: projects, isError, error } = useGetAllProjectsQuery(page);

  const pPage = projects ? projects.page : 0;
  const pPerPage = projects ? projects.perPage : 0;
  const pCount = projects ? projects.count : 0;

  const viewPageItem = pPage * pPerPage - (pPerPage - 1);
  const viewNextPageItem = pPage * pPerPage;

  //const paginateProjects = flow(chunk(pPerPage), get(page - 1), slice(0, pPerPage), map(ProjectTableRow));
  //const projectRows = useSelector((state: RootState) => paginateProjects(state.projects.items));

  if (isError) {
    console.error(error);
    return <Box>{error.toString()}</Box>;
  }

  if (!projects?.data) {
    return <Box>No projects found</Box>;
  }

  return (
    <Container py={{ base: '4', md: '8' }} px={{ base: '0', md: 8 }}>
      <Box bg="bg.surface" boxShadow={{ base: 'none', md: 'sm' }} borderRadius={{ base: 'none', md: 'lg' }}>
        <Stack spacing="5">
          <Box px={{ base: '4', md: '6' }} pt="5">
            <Stack direction={{ base: 'column', md: 'row' }} justify="space-between">
              <Text textStyle="lg" fontWeight="medium">
                Members
              </Text>
              <InputGroup maxW="xs">
                <InputLeftElement pointerEvents="none">
                  <Icon as={FiSearch} color="fg.muted" boxSize="5" />
                </InputLeftElement>
                <Input placeholder="Search" />
              </InputGroup>
            </Stack>
          </Box>
          <Box overflowX="auto">
            <Table>
              <Thead>
                <Tr>
                  <Th>
                    <HStack spacing="3">
                      <Checkbox />
                      <Text>Name</Text>
                    </HStack>
                  </Th>
                  <Th>Description</Th>
                  <Th> </Th>
                </Tr>
              </Thead>
              <ProjectsTable page={page} />
            </Table>
          </Box>
          <Box px={{ base: '4', md: '6' }} pb="5">
            <HStack spacing="3" justify="space-between">
              {!isMobile && (
                <Text color="fg.muted" textStyle="sm">
                  {`Showing ${viewPageItem} to ${viewNextPageItem} of ${pCount} results`}
                </Text>
              )}
              <ButtonGroup
                spacing="3"
                justifyContent="space-between"
                width={{ base: 'full', md: 'auto' }}
                variant="secondary"
              >
                <Button
                  isDisabled={page <= 1}
                  onClick={() => {
                    setPage(page - 1);
                  }}
                >
                  Previous
                </Button>
                <Button
                  isDisabled={viewNextPageItem >= pCount}
                  onClick={() => {
                    setPage(page + 1);
                  }}
                >
                  Next
                </Button>
              </ButtonGroup>
            </HStack>
          </Box>
        </Stack>
      </Box>
    </Container>
  );
};

export const Projects = () => (
  <Box as="main" role="main" bg="bg.surface" pt={{ base: '4', md: '8' }} pb={{ base: '12', md: '24' }}>
    <Container>
      <Stack spacing="4">
        <Stack spacing="4" direction={{ base: 'column', sm: 'row' }} justify="space-between">
          <Stack spacing="1">
            <Heading size={{ base: 'xs', md: 'sm' }} fontWeight="medium">
              Projects
            </Heading>
          </Stack>
          <Stack direction="row" spacing="3">
            <Button variant="secondary">Share</Button>
            <Button>Create</Button>
          </Stack>
        </Stack>
      </Stack>
      <ProjectsTableContainer />
    </Container>
  </Box>
);
