import { Box, Button, Container, Stack, Heading, Text } from '@chakra-ui/react';
import { Link as RRLink } from 'react-router-dom';

export const Home = () => (
  <Box
    as="main"
    role="main"
    bg="bg.surface"
    pt={{ base: '4', md: '8' }}
    pb={{ base: '12', md: '24' }}
  >
    <Container>
      <Stack spacing={{ base: '8', md: '10' }}>
        <Stack spacing={{ base: '4', md: '6' }}>
          <Stack spacing="3">
            <Text
              fontSize={{ base: 'sm', md: 'md' }}
              fontWeight="medium"
              color="accent"
            >
              Welcome!
            </Text>
            <Heading size={{ base: 'md', md: 'lg' }} fontWeight="semibold">
              Home
            </Heading>
          </Stack>
          <Text color="fg.muted" fontSize={{ base: 'lg', md: 'xl' }} maxW="3xl">
            This is an agile project management tool based on JIRA.
          </Text>
        </Stack>
        <Stack direction={{ base: 'column-reverse', md: 'row' }}>
          <Button variant="secondary" size="xl">
            Learn more
          </Button>
          <Button as={RRLink} to="/projects" size="xl">
            Projects
          </Button>
        </Stack>
      </Stack>
    </Container>
  </Box>
);
