import {
  extendTheme,
  defineStyle,
  defineStyleConfig,
  defineCssVars,
} from '@chakra-ui/react';
import { theme } from '@chakra-ui/pro-theme';

const btnVars = defineCssVars('button', ['bg', 'color']);

const proTheme = extendTheme(theme);
const Button = defineStyleConfig({
  ...proTheme.components.Button,
  baseStyle: {
    ...proTheme.components.Button.baseStyle,
    borderRadius: 'none',
  },
  variants: {
    primary: defineStyle((props) => {
      const { colorScheme } = props;

      return {
        ...proTheme.components.Button.variants.primary,
        borderRadius: 'none',
        _dark: {
          [btnVars.bg.variable]: `colors.${colorScheme}.500`,
          [btnVars.color.variable]: 'colors.white',
        },
        _hover: {
          [btnVars.bg.variable]: `colors.${colorScheme}.600`,
          _disabled: {
            background: 'inerit',
          },
          _dark: {
            [btnVars.bg.variable]: `colors.${colorScheme}.600`,
          },
        },
        _active: {
          [btnVars.bg.variable]: `colors.${colorScheme}.700`,
          _dark: {
            [btnVars.bg.variable]: `colors.${colorScheme}.700`,
          },
        },
        _disabled: {
          _hover: {
            [btnVars.bg.variable]: `colors.${colorScheme}.500`,
            _dark: {
              [btnVars.bg.variable]: `colors.${colorScheme}.200`,
            },
          },
        },
      };
    }),
    secondary: defineStyle({
      ...proTheme.components.Button.variants.secondary,
      borderRadius: 'none',
    }),
    tertiary: defineStyle({
      ...proTheme.components.Button.variants.tertiary,
      borderRadius: 'none',
    }),
  },
});

const config = {
  initialColorMode: 'system',
  useSystemColorMode: true,
};

const blue = {
  '50': '#E5E7FF',
  '100': '#B8BCFF',
  '200': '#8A91FF',
  '300': '#5C65FF',
  '400': '#2E3AFF',
  '500': '#000FFF',
  '600': '#000CCC',
  '700': '#000999',
  '800': '#000666',
  '900': '#000333',
};

const extendedConfig = extendTheme({
  config,
  colors: {
    ...proTheme.colors,
    blue,
    brand: blue,
  },
  components: {
    ...proTheme.components,
    Button,
  },
});
const appTheme = extendTheme(extendedConfig, proTheme);

export default appTheme;
