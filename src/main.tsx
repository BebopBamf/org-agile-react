import React from 'react';
import ReactDOM from 'react-dom/client';

import '@fontsource-variable/open-sans';
import '@fontsource-variable/spline-sans';

import { Provider } from 'react-redux';
import { ChakraProvider, ColorModeScript } from '@chakra-ui/react';

import appTheme from './app-theme';
import store from './store';

import App from './App.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <ColorModeScript initialColorMode={appTheme.config.initialColorMode} />
    <ChakraProvider theme={appTheme}>
      <Provider store={store}>
        <App />
      </Provider>
    </ChakraProvider>
  </React.StrictMode>,
);
